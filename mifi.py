import os

from huawei_lte_api.Client import Client
from huawei_lte_api.AuthorizedConnection import AuthorizedConnection
from huawei_lte_api.enums.sms import BoxTypeEnum

connection = AuthorizedConnection('http://192.168.8.1/', username="admin", password=os.environ['B525_PASSWORD'])
client = Client(connection)

print(client.monitoring.traffic_statistics())
all_messages = []

#Cycle through all boxes and list messages
for my_current_boxtype in BoxTypeEnum:
    print("List all message in box {}".format(my_current_boxtype.name))
    results = client.sms.get_sms_list(box_type=my_current_boxtype)
    if results["Messages"] is None:
        continue
    found_messages = results["Messages"]['Message']
    if not isinstance(found_messages, list):
        all_messages.append(dict(found_messages))
    else:
        all_messages = all_messages + found_messages

print("Found {} message(s)".format(len(all_messages)))

for message in all_messages:
    # Confirmation by Tele2 that new allowance is set; delete all SMS in the box and reset traffic count
    if "Gelukt! U kunt weer" in message["Content"]:
        print('request for extra data confirmed, delete all messages & reset counter')
        for msg_for_id in all_messages:
            print('delete SMS {}'.format(msg_for_id['Index']))
            # use internal connection because of bug in delete-sms implementation;
            # bug: will convert list to dict in XML message, dict in msg not supported
            # correct examples here:
            # <?xml version: "1.0" encoding="UTF-8"?><request><Index>40003</Index></request>
            # <?xml version: "1.0" encoding="UTF-8"?><request><Index>40006</Index><Index>40000</Index></request>
            # workaround, send dict ourselves using 1 index per call.
            client.sms._connection.post('sms/delete-sms', {'Index': msg_for_id['Index']})
        client.monitoring.set_clear_traffic()
        client.sms.send_sms([os.environ['EXTRA_DATA_SMS']], "1 GB erbij voor Dwingeloo")
        exit(0)

    # Not a lot data left, let's ask for more!
    if "U heeft nog 500MB over van uw dagbundel" in message["Content"]:
        # When 500Mb left, send SMS
        print("Less than 500Meg to go, better ask for more by sending a SMS!")
        client.sms.send_sms(["1280"], "1GB Extra")

