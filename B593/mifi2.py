import huaweisms.api.user
import huaweisms.api.wlan
import huaweisms.api.sms

ctx = huaweisms.api.user.quick_login("admin", "mypassword", "192.168.1.1")
print(ctx)
# output: <ApiCtx modem_host=192.168.8.1>

# sending sms
huaweisms.api.sms.send_sms(
    ctx,
    '+31612345678"
    'this is the sms message'
)

# connected devices
device_list = huaweisms.api.wlan.get_connected_hosts(ctx)
